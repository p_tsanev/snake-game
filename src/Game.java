import javax.swing.JPanel;
import javax.swing.Timer;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Game extends JPanel{
	private int width, height;
	final private int units = 30;

	public int getScore() {
		return score;
	}

	private int score = 0;
	private final int highScore;
	public boolean  newRecord = false;

	public int getSpeed() {
		return speed;
	}

	private int speed = units;

	//Timer used to refresh the game
	Timer timer;

	ArrayList<Entity> player = new ArrayList<>();
	
	Entity fruit;

	TextureManager textures;

	Game(int width, int height, TextureManager textures) throws IOException {
		this.textures = textures;

		Scanner scan = new Scanner(new File("./src/configs/scores.txt"));
		String info = scan.nextLine();
		highScore = Integer.parseInt(info.split(" ")[1]);

		System.out.println("Last high was" + highScore);

		setBackground(Color.black);

		setW(width);
		setH(height);


		player.add(new Entity(width/2 + units, height/2 + units));
		fruit = new Entity((randomizer(width - 30, 30)/units) * units, (randomizer(height - 30, 30)/units) * units);

		int refreshRate = 85;
		timer = new Timer(refreshRate, e -> repaint());

		timer.start();
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		map(g); // Generate the map lines and the walls
		update(g); // Draw the player and move him
		canSpawn();
		selfCollide();
	}

	public void map(Graphics g){
		for(int i = 0; i < height/units; i++) {
			for(int j = 0; j < width/units; j++) {
				if(i == 0 || i == height/units - 1 || j == 0 || j == width/units - 1){
					g.drawImage(textures.wall, j*units, i*units, null);
				}
			}
		}

	}

	public void update(Graphics g) {
		for(int i = 0; i < player.size(); i++) {
			if(i == 0)g.drawImage(textures.snakeHead, player.get(i).getX(), player.get(i).getY() , null);
			else g.drawImage(textures.snakeBody, player.get(i).getX(), player.get(i).getY(), null);
		}
		for(int i = player.size() - 1; i > 0; i--){
			player.get(i).setX(player.get(i - 1).getX());
			player.get(i).setY(player.get(i - 1).getY());
		}

		g.drawImage(textures.apple, fruit.getX(), fruit.getY(), null);

		switch(player.get(0).getDirection()) {
			case "up" -> {
				if((player.get(0).getY() - speed) != 0){
					player.get(0).prevY = player.get(0).getY();
					player.get(0).setY(player.get(0).getY() - speed);
				}
				else {
					die();
				}
			}
			case "down" -> {
				if((player.get(0).getY() + speed) != height - units){
					player.get(0).prevY = player.get(0).getY();
					player.get(0).setY(player.get(0).getY() + speed);
				}
				else {
					die();
				}
			}
			case "right" ->{
				if((player.get(0).getX() + speed) != width - units){
					player.get(0).prevX = player.get(0).getX();
					player.get(0).setX(player.get(0).getX() + speed);
				}
				else {
					die();
				}
			}
			case "left" ->{
				if((player.get(0).getX() - speed) != 0){
					player.get(0).prevX = player.get(0).getX();
					player.get(0).setX(player.get(0).getX() - speed);
				}
				else {
					die();
				}
			}
		}

		if(player.get(0).getX() == fruit.getX() && player.get(0).getY() == fruit.getY()) {
			score++;
			fruit.setX((randomizer(width - 30, 30)/units) * units);
			fruit.setY((randomizer(height - 30, 30)/units) * units);

			player.add(new Entity(width + units, height + units));
		}

	}

	public void selfCollide(){
		for(int i = 1; i < player.size(); i++) {
			if(player.get(0).getX() == player.get(i).getX() && player.get(0).getY() == player.get(i).getY()){
				die();
			}
		}
	}

	public int randomizer(int max, int min){
		return (int) Math.floor(Math.random() * (max - min) + min);
	}

	public void canSpawn(){
		for (Entity entity : player) {
			if (entity.getX() == fruit.getX() && entity.getY() == fruit.getY()) {
				fruit.setX((randomizer(width - 30, 30) / units) * units);
				fruit.setY((randomizer(height - 30, 30) / units) * units);
			}
		}
	}

	public Dimension getPreferredSize() {
		if (isPreferredSizeSet()) {
			return super.getPreferredSize();
		}
		return new Dimension(width, height);
	}

	public void die() {
		speed = 0;
		timer.stop();

		if(highScore < score) {
			newRecord = true;
			File progress = new File("D:\\Programming\\snake-game\\src\\configs\\scores.txt");
			try {
				FileWriter fw = new FileWriter(progress);
				fw.write("HighScore: " + score);
				System.out.println("New high is " + score);
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void setW(int width) {
		this.width = width;
	}

	public void setH(int height) {
		this.height = height;
	}
}
