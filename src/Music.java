import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class Music extends MyAudioPlayer{

    Music(){
        try {
            inputSound = AudioSystem.getAudioInputStream(new File(path).getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(inputSound);
            clip.loop(Clip.LOOP_CONTINUOUSLY);

            fc = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);

        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }

    }
}
