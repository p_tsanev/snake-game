import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Objects;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;

public class Main extends JFrame{
	//Size of the entire window
	private final int windowW = 650;
	private final int windowH = 700;

	TextureManager textures = new TextureManager();

	Music player;

	
	Main() throws IOException {
		//Method to generate the window
		initWindow(this);

		//Init the main menu of the game
		MainMenu menu = new MainMenu(windowW, windowH, player, textures);

		final PlayMenu[] snake = new PlayMenu[1]; //Predefine the game panel with an array because of lambda function


		//A listener for key events - presses of the W, A, S, D keys
		addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				char key = e.getKeyChar();

				switch(key) {
					//Use of the .equals method of the object class
					case 'w' -> {
						if(!Objects.equals(snake[0].game.player.get(0).getDirection(), "down")) snake[0].setDirection("up");
					}
					case 's' -> {
						if(!Objects.equals(snake[0].game.player.get(0).getDirection(), "up")) snake[0].setDirection("down");
					}
					case 'a' -> {
						if(!Objects.equals(snake[0].game.player.get(0).getDirection(), "right")) snake[0].setDirection("left");
					}
					case 'd' ->{
						if(!Objects.equals(snake[0].game.player.get(0).getDirection(), "left")) snake[0].setDirection("right");
					}
				}

				for(int i = 0; i < snake[0].game.player.size(); i++) {
					snake[0].game.player.get(i).setDirection(snake[0].getDirection());
				}

			}

		});

		add(menu);
		menu.startButton.addActionListener(e -> {
			try {
				snake[0] = new PlayMenu(windowW, windowH, textures); //Starting the game panel
				remove(menu);
				add(snake[0]);
				revalidate();
				repaint();
				requestFocus();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		});

		setVisible(true); //Making the contents of the frame visible
	}

	//A method to initialize the JFrame and clear up the constructor
	public void initWindow(JFrame frame){
		frame.setTitle("Snake Game 2D");

		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.lightGray);
		frame.setSize(windowW, windowH);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setLayout(null);

		player = new Music();
		try {
			player.clip.open(); // Starting the music
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	//Homework
	public String toString(){
		return "My game";
	}
	
	public static void main(String[] args){
		try {
			new Main();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
