
public class Entity {
	private int x, y;
	public int prevX, prevY;
	private String direction = "up";
	
	Entity(int x, int y){
		setX(x);
		setY(y);
		this.prevX = x;
		this.prevY = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
