import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class PlayMenu extends JPanel {
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    private String direction; // Variable holding and keeping track of the direction of the player

    TextureManager textures; //Texture object with preloaded images

    Game game;

    PlayMenu(int windowW, int windowH, TextureManager textures) throws IOException {
        int GAME_WIDTH = 600;
        int GAME_HEIGHT = 600;

        this.textures = textures;

        setBounds(0, 0, windowW, windowH);
        setLayout(null);

        //Creation and positioning of the panel with the game
        game = new Game(GAME_WIDTH, GAME_HEIGHT, textures);
        game.setBounds(20, 50, GAME_WIDTH, GAME_HEIGHT);

        //Generating and positioning the label object displaying the score
        JLabel text = new JLabel("Score: " + game.getScore());
        text.setBounds(windowW/2 - 80, 10, 150, 40);
        text.setFont(new Font("Georgia", Font.BOLD, 30));

        //A timer firing every half a second to update the score on the screen
        Timer timer = new Timer(500, e ->{
            if(game.getSpeed() == 0){
                if(game.newRecord) {
                    text.setText("New record!");
                } else {
                    text.setText("You have died!");
                }
                text.setSize(300, 40);
            } else {
                text.setText("Score: " + game.getScore());
            }
        });
        timer.start();

        //Add the components - game panel and the score counting text
        add(game);
        add(text);

        setVisible(true);
    }
}
