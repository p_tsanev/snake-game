import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

//Abstract classes homework
public abstract class MyAudioPlayer {
    //Path to the song
    String path = "./src/resources/audio/game_song.wav";
    ///Clip object used to run the song
    Clip clip;
    AudioInputStream inputSound;
    //Current volume variable
    int currentVolume;
    //Float control object used to change the volume
    FloatControl fc;
}
