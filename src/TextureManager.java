import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

//A class to preload all image resources
public class TextureManager {
    BufferedImage apple;
    BufferedImage snakeHead;
    BufferedImage snakeBody;
    BufferedImage wall;
    BufferedImage menu;

    public TextureManager() throws IOException {
        apple = ImageIO.read(getClass().getResource("./resources/images/apple.png"));
        snakeBody = ImageIO.read(getClass().getResource("./resources/images/body.png"));
        snakeHead = ImageIO.read(getClass().getResource("./resources/images/head.png"));
        wall = ImageIO.read(getClass().getResource("./resources/images/wall.png"));
        menu = ImageIO.read(getClass().getResource("./resources/images/gamebackground.png"));
    }
}
