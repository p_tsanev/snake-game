import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class MainMenu extends JPanel {
    //Local width and height variables
    private int width;
    private int height;

    //Components for the title, start button, slider for the volume, textures object and a music player
    JLabel appTitle;
    JButton startButton;
    JSlider volumeSlider;
    TextureManager textures;
    Music player;

    MainMenu(int windowW, int windowH, Music player, TextureManager textures){
        width = windowW;
        height = windowH;
        this.player = player;
        this.textures = textures;

        setBounds(0, 0, width, height);
        setBackground(Color.GREEN);
        setLayout(null);

        appTitle = new JLabel("Snake Game");
        appTitle.setBounds(width/2 - 150, 100, 350, 50);
        appTitle.setFont(new Font("Georgie", Font.BOLD, 50));

        startButton = new JButton("Start");
        startButton.setBounds(width/2 - 100, 250, 200, 50);
        startButton.setFont(new Font("Georgie", Font.BOLD, 20));

        volumeSlider = new JSlider(-40, 6);
        volumeSlider.setBounds(width/2 - 150, 450, 300, 100);
        volumeSlider.setBackground(Color.GREEN);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(textures.menu, 0, 0, null);
        addUI(player);
    }

    //To add the components on top of the main menu image
    void addUI(Music player){
        add(appTitle);
        add(startButton);
        add(volumeSlider);
        volumeSlider.addChangeListener(e -> {
            player.currentVolume = volumeSlider.getValue();
            System.out.println(volumeSlider.getValue());
            player.fc.setValue(player.currentVolume);
        });
    }
}
